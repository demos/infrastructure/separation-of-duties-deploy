# Separation of Duties

Within the constraints of Premium/Silver, how do you deal with separation of duties? 

This project is used to separate the GitLab YAML from the codebase
that developers use to build their software in order to maintain
separation of duties.

#### Context

Our company has two roles, `developers` and `deployers`.  

* Developers can only modify source code for applications. Deployers can only modify deployment scripts. 
* Developers can only deploy to development and staging. Deployers can deploy to development, staging, and production.

#### What This Project Contains

* Application Source Code

#### How we implement Separation of Duties

*All actions below are done by a GitLab Maintainer*

1) Use the `raw` location of the `deploy.gitlab-ci.yml` and place it in the `Separation of Duties` project's `Settings` > `CI / CD` > `General Pipelines` > `Custom CI configuration path`
1) Grant `Developer` access for the developers to the `Separation of Duties` project. Grant `Guest` access for develoeprs to the `Separation of Duties Deploy` project.
1) Grant `Maintainer` access for the deployers to the `Separation of Duties` project (this is so that they can play jobs in pipelines initiated by other users). Grant `Developer` access for the deployers to the `Separation of Duties Deploy` project.
1) Create the future environments (per the names defined in the `deploy.gitlab-ci.yml` file in the `Separation of Duties Deploy` project) if they do not exist in the `Separation of Duties` project (going to `Operations` > `Environments` > `New`).
1) In `Settings` > `CI / CD` > `Protected Environments` of the `Separation of Duties` project, only allow the Deployment User(s) access to deploy to production
1) In `Settings` > `CI / CD` > `Protected Branches` of the `Separation of Duties` project, only allow the Development User(s) access to push to master
1) In `Settings` > `CI / CD` > `Protected Branches` of the `Separation of Duties Deploy` project, only allow the Deployment User(s) access to push to master


#### Useful Links

* [Separation of Duties Deploy Members](https://gitlab.com/gitlab-silver/tpoffenbarger/separation-of-duties-deploy/-/project_members?utf8=%E2%9C%93&search=tpoffenbarger)
* [Separation of Duties Members](https://gitlab.com/gitlab-silver/tpoffenbarger/separation-of-duties/-/project_members?utf8=%E2%9C%93&search=tpoffenbarger)
* [Separation of Duties Repository Settings](https://gitlab.com/gitlab-silver/tpoffenbarger/separation-of-duties/-/settings/repository)
* [Separation of Duties CI/CD Settings](https://gitlab.com/gitlab-silver/tpoffenbarger/separation-of-duties/-/settings/ci_cd)
* [Separation of Duties Environments](https://gitlab.com/gitlab-silver/tpoffenbarger/separation-of-duties/-/environments)
